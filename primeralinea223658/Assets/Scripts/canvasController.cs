﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class canvasController : MonoBehaviour
{

    public Image imgSeleccionar;

    public Slider regulador;

    public void cambiarColor (bool colorActual)
    {
        if(colorActual == true)
        {
            imgSeleccionar.color = Color.green;
        }
        else
        {
            imgSeleccionar.color = Color.blue;
        }
    }

    public void ocultarCursor (bool ocultar)
    {
        imgSeleccionar.enabled = !ocultar;
    }

    public void activarSlider (bool activar)
    {
        regulador.gameObject.SetActive(activar);
    }
    public void establecerValorSlider (float sliderValor)
    {
        regulador.value = sliderValor;
    }
}
