﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class test : MonoBehaviour
{

    public GameObject esfera;
    public Transform camara;
    public float distancia = 2f;

    public float fuerzaEsfera = 250f;
    public bool tieneLaEsfera = true;
    Rigidbody esferaRB;

    bool isPickable = false;
    public canvasController scriptCanvas;
    public LayerMask capaLayer;
    RaycastHit colision;

    public float ballForceMin = 150f;
    public float ballForceMax = 900f;
    public float ballForce;
    public float totalTimer = 3f;
    float tiempoTranscurrido = 0.0f;

    TrailRenderer esferaTrail;



    void Start()
    {
        esferaRB = esfera.GetComponent<Rigidbody>();
        //esferaRB = esferas[0].GetComponent<Rigidbody>();
        esferaRB.useGravity = false;
        scriptCanvas.ocultarCursor(true);
        scriptCanvas.activarSlider(false);
        esferaTrail = GetComponent<TrailRenderer>();
        esferaTrail.enabled = false;
    }


    void Update()
    {
        if (tieneLaEsfera)
        {

            if (Input.GetMouseButtonDown(0))
            {
                tiempoTranscurrido = 0.0f;
                
                scriptCanvas.establecerValorSlider(0);
                scriptCanvas.activarSlider(true);
                
            }

            if (Input.GetMouseButton(0))
            {
                tiempoTranscurrido += Time.deltaTime;
                ballForce = Mathf.Lerp(ballForceMin, ballForceMax, tiempoTranscurrido / totalTimer);
                scriptCanvas.establecerValorSlider(tiempoTranscurrido / totalTimer);
            }

            if (Input.GetMouseButtonUp(0))
            {
                tieneLaEsfera = false;
                esferaRB.useGravity = true;
                esferaRB.AddForce(camara.forward * ballForce);
                scriptCanvas.ocultarCursor(false);
                ballForce = 0.0f;
                scriptCanvas.activarSlider(false);
                esferaTrail.enabled = true;
            }
        }
        else
        {

            if (Physics.Raycast(camara.position, camara.TransformDirection(Vector3.forward), out colision, Mathf.Infinity, capaLayer))
            {
                if (isPickable == false)
                {
                    isPickable = true;
                    scriptCanvas.cambiarColor(true);
                }
                if (isPickable && Input.GetKeyDown(KeyCode.E))
                {
                    tieneLaEsfera = true;
                    esferaRB.useGravity = false;
                    esferaRB.velocity = Vector3.zero;
                    esferaRB.angularVelocity = Vector3.zero;
                    esfera.transform.localRotation = Quaternion.identity;
                    //esferas[0].transform.localRotation = Quaternion.identity;
                    //reglas.instance.puntoValido = false;
                    scriptCanvas.cambiarColor(true);
                    scriptCanvas.ocultarCursor(true);
                    esferaTrail.enabled = false;
                }
            }
            else if (isPickable)
            {
                isPickable = false;
                scriptCanvas.cambiarColor(false);
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("menu");
        }
    }

    private void LateUpdate()
    {
        if (tieneLaEsfera)
        {
            esfera.transform.position = camara.position + camara.forward * distancia;
            //esferas[0].transform.position = camara.position + camara.forward * distancia;
        }
    }
}
