using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logica1 : MonoBehaviour
{
    public float movementSpeed = 5.0f;
    public float rotationSpeed = 200.0f;
    private Animator anim;
    public float x, y;

    bool tirar;
    bool tirando;


    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        if(Input.GetKey("x"))
        {
            tirando = true;
        }

        else
        {
            tirando = false;
            
        }
                
        
        

        
      /*  transform.Rotate(0, x * Time.deltaTime * rotationSpeed, 0);*/
        transform.Translate(x, 0, y * Time.deltaTime * movementSpeed);

        anim.SetFloat("VelX", x);
        anim.SetFloat("VelY", y);

        
        anim.SetBool("tirando", tirando);

    }
}
