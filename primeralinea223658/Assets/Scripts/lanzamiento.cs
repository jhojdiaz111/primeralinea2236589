﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lanzamiento : MonoBehaviour
{

    //public GameObject artefacto;
    public GameObject[] artefactos;
    public Transform camaraVJF;
    public float distancia = 3f;

    public float fuerzaLaunch = 200f;
    bool tieneArtefacto = true;
    Rigidbody artefactoRB;
    

    bool coleccionar = false;
    public canvasController scriptCanvas;
    public LayerMask capaColeccion;
    RaycastHit hit;
    

    void Start()
    {
        //artefactoRB = artefacto.GetComponent<Rigidbody>();
        artefactoRB = artefactos[0].GetComponent<Rigidbody>();
        artefactoRB.useGravity = false;
        scriptCanvas.ocultarCursor(true);

    }

    void Update()
    {
        if (tieneArtefacto == true)
        {
            

            if(Input.GetMouseButtonDown(0))
            {
                tieneArtefacto = false;
                artefactoRB.useGravity = true;
                artefactoRB.AddForce(camaraVJF.forward * fuerzaLaunch);
                scriptCanvas.ocultarCursor(false);
            }
            else
            {
                if (Physics.Raycast(camaraVJF.position, camaraVJF.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, capaColeccion))
                {
                    /*
                    if (Input.GetKeyDown(KeyCode.X))
                    {
                        print("HolaX");
                    }
                    */

                    if (coleccionar == false)
                    {
                        coleccionar = true;
                        scriptCanvas.cambiarColor(true);
                    }
                    if (coleccionar == true && Input.GetKeyDown(KeyCode.E))
                    {
                        coleccionar = true;
                        artefactoRB.useGravity = false;
                        artefactoRB.velocity = Vector3.zero;
                        artefactoRB.angularVelocity = Vector3.zero;
                        //artefacto.transform.localRotation = Quaternion.identity;
                        artefactos[0].transform.localRotation = Quaternion.identity;

                        scriptCanvas.cambiarColor(true);
                        scriptCanvas.ocultarCursor(true);
                    }
                } 
                else if (coleccionar == true)
                {
                    coleccionar = false;
                    scriptCanvas.cambiarColor(false);
                }
                
            }
        }

    }

    private void LateUpdate()
    {
        if (tieneArtefacto == true)
        {
            //artefacto.transform.position = camaraVJF.position + camaraVJF.forward * distancia;
            artefactos[0].transform.position = camaraVJF.position + camaraVJF.forward * distancia;
        }
    }

}
