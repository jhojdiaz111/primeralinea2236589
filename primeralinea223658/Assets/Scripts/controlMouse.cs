﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlMouse : MonoBehaviour
{
    
    public float velocidadMouse = 100f;

    public Transform camara;

    float mouseX;
    float mouseY;

    float yMove;



    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        mouseX = Input.GetAxis("Mouse X") * velocidadMouse * Time.deltaTime;
        mouseY = Input.GetAxis("Mouse Y") * velocidadMouse * Time.deltaTime;
        yMove -= mouseY;


        yMove = Mathf.Clamp(yMove, -90f, 90f);
        camara.localRotation = Quaternion.Euler(yMove, 0, 0);
        transform.Rotate(Vector3.up * mouseX);
    }
}
